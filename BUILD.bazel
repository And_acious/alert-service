load("@rules_proto//proto:defs.bzl", "proto_library")
load("@io_bazel_rules_go//proto:def.bzl", "go_proto_library")
load("@rules_proto_grpc//java:defs.bzl", "java_grpc_library")
load("@io_bazel_rules_go//go:def.bzl", "go_binary", "go_library", "go_test")
load("@io_bazel_rules_docker//go:image.bzl", "go_image")
load("//tools/kubernetes:gen.bzl", "k8s_go")

go_image(
    name = "alert-service-image",
    binary = ":alert-service",
    importpath = "geniusrx.dev/geniusrx/alert-service",
)

k8s_go(
    name = "deploy",
    grpc_port = "9006",
    http_port = "8006",
    image_name = "alert-service",
    image_target = ":alert-service-image",
)

proto_library(
    name = "pb_proto",
    srcs = ["messages.proto"],
    visibility = ["//visibility:public"],
    deps = ["@com_google_protobuf//:empty_proto"],
)

go_proto_library(
    name = "pb_go_proto",
    compilers = ["@io_bazel_rules_go//proto:go_grpc"],
    importpath = "geniusrx.dev/geniusrx/alert-service/pb",
    proto = ":pb_proto",
    visibility = ["//visibility:public"],
)

java_grpc_library(
    name = "alert_service_java_grpc",
    visibility = ["//visibility:public"],
    deps = [":pb_proto"],
)

go_library(
    name = "alert-service_lib",
    srcs = [
        "alert.go",
        "http.go",
        "main.go",
    ],
    importpath = "geniusrx.dev/geniusrx/alert-service",
    visibility = ["//visibility:private"],
    deps = [
        ":pb_go_proto",
        "//go/grpc",
        "//go/logging",
        "//go/tracing",
        "@com_github_spf13_viper//:viper",
        "@in_gopkg_datadog_dd_trace_go_v1//contrib/gorilla/mux",
        "@io_bazel_rules_go//proto/wkt:empty_go_proto",
        "@org_uber_go_zap//:zap",
    ],
)

go_binary(
    name = "alert-service",
    embed = [":alert-service_lib"],
    visibility = ["//visibility:public"],
)

go_test(
    name = "alert-service_test",
    size = "small",
    srcs = [
        "alert_test.go",
        "http_test.go",
    ],
    embed = [":alert-service_lib"],
    deps = [
        "@com_github_stretchr_testify//assert",
        "@com_github_stretchr_testify//mock",
        "@org_uber_go_zap//:zap",
    ],
)
