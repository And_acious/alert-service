package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"geniusrx.dev/geniusrx/go/logging"
	"go.uber.org/zap"
	"io/ioutil"
	"net/http"
	"sort"
	"strings"
)

type AlertService interface {
	PushApiAlertToSlack(ctx context.Context, alertMetaData map[string]string, alertWebhookUrl string, alertingApplicationName string) error
	ComposeSlackMessageBlocks(metaData map[string]string) []SlackMessageBlock
}

type alertService struct {
}

type SlackMessageText struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

type SlackMessageBlock struct {
	Type string           `json:"type"`
	Text SlackMessageText `json:"text"`
}

type SimpleSlackAlertMessage struct {
	Text      string              `json:"text"`
	IconEmoji string              `json:"icon_emoji"`
	Blocks    []SlackMessageBlock `json:"blocks"`
}

//TODO: Figure out why Go can't seem to unmarshall the response from Slack to this type
type SlackResponse struct {
	Body string `json:"Body"`
}

func NewAlertService() AlertService {
	return &alertService{}
}

func (a *alertService) PushApiAlertToSlack(ctx context.Context, alertMetaData map[string]string, alertWebhookUrl string, alertingApplicationName string) error {
	l := logging.LoggerFromContext(ctx)
	l.Debug("Pushing Alert to Slack!")
	messageBlocks := a.ComposeSlackMessageBlocks(alertMetaData)
	alertMessage := SimpleSlackAlertMessage{
		Text:      fmt.Sprintf("%s has thrown an alert!", alertingApplicationName),
		IconEmoji: ":bangbang:",
		Blocks:    messageBlocks,
	}

	requestBody, err := json.Marshal(alertMessage)
	if err != nil {
		l.Error("Unable to marshall Slack Alert Message!", zap.Error(err))
		return err
	}

	resp, err := http.Post(alertWebhookUrl, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		l.Error("Unable to post Alert Message to Slack!", zap.Error(err))
		return err
	}

	defer resp.Body.Close()
	//var slackResponse SlackResponse
	b, err := ioutil.ReadAll(resp.Body)
	l.Debug("Response body from Alert post to Slack:", zap.String("Body", string(b)))

	if !strings.Contains(string(b), "ok") {
		l.Error("Unable to post Alert Message to Slack! Slack API returned: ", zap.String("Body", string(b)))
		l.Error("Request Body for sending Alert message to Slack that failed", zap.String("Failed Request Body", string(requestBody)))
		return errors.New(fmt.Sprintf("Unable to post Alert Message to Slack! Slack API returned: %s", string(b)))
	}

	l.Debug("Sent the following alert message successfully to Slack: ", zap.String("Successful Request Body", string(requestBody)))
	return nil
}

func (a *alertService) ComposeSlackMessageBlocks(metaData map[string]string) []SlackMessageBlock {
	blockList := make([]SlackMessageBlock, 0)
	sortedKeys := a.sortMetaDataKeysAlphabetically(metaData)

	for _, k := range sortedKeys {
		txt := SlackMessageText{}
		v := metaData[k]
		if v == "" {
			continue
		}
		if len(blockList) == 0 {
			txt = a.composeSlackMessageText(fmt.Sprintf("@channel %s", k), v)
		} else {
			txt = a.composeSlackMessageText(k, v)
		}

		blockList = append(blockList, SlackMessageBlock{
			Type: "section",
			Text: txt,
		})
	}

	return blockList
}

/*
	Since iterating over a map in Go doesn't guarantee order, sort the keys alphabetically and then use them
	to pull values out of the meta data map
*/
func (a *alertService) sortMetaDataKeysAlphabetically(metaData map[string]string) []string {
	var sortedKeys []string
	for key := range metaData {
		sortedKeys = append(sortedKeys, key)
	}

	sort.Strings(sortedKeys)
	return sortedKeys
}

func (a *alertService) composeSlackMessageText(textTitle string, textValue string) SlackMessageText {
	//Remove any quote characters in text value message that would break when posting to Slack API
	textValue = strings.ReplaceAll(textValue, "\"", "")

	//Trimming text here as Slack supports (from what I can tell) a maximum of 40,000 characters per text entry
	if len(textValue) > 20000 {
		textValue = textValue[0:20000]
	}
	msgText := fmt.Sprintf("*%s*: ```%s```", textTitle, textValue)
	return SlackMessageText{
		Type: "mrkdwn",
		Text: msgText,
	}
}
