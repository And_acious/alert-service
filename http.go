package main

import (
	"context"
	"encoding/json"
	"geniusrx.dev/geniusrx/alert-service/pb"
	"geniusrx.dev/geniusrx/go/logging"
	emptypb "github.com/golang/protobuf/ptypes/empty"
	"go.uber.org/zap"
	muxtrace "gopkg.in/DataDog/dd-trace-go.v1/contrib/gorilla/mux"
	"net/http"
)

type WebServer struct {
	AlertService          AlertService
	uiWebhookUrl          string
	dsWebhookUrl          string
	apiWebhookUrl         string
	EnableUiSlackAlerting bool
	EnableDsSlackAlerting bool
}

func NewWebServer(alertService AlertService, uiWebhookUrl string, dsWebhookUrl string, apiWebhookUrl string, enableUiSlackAlerting bool, enableDsSlackAlerting bool) *WebServer {
	return &WebServer{
		AlertService:          alertService,
		uiWebhookUrl:          uiWebhookUrl,
		dsWebhookUrl:          dsWebhookUrl,
		apiWebhookUrl:         apiWebhookUrl,
		EnableUiSlackAlerting: enableUiSlackAlerting,
		EnableDsSlackAlerting: enableDsSlackAlerting,
	}
}

func (ws *WebServer) Router() *muxtrace.Router {
	r := muxtrace.NewRouter()
	s := r.PathPrefix("/alert").Subrouter()

	s.Methods("POST").Path("/ui").HandlerFunc(ws.CreateUiAlert)
	s.Methods("POST").Path("/ds").HandlerFunc(ws.CreateDsAlert)

	return r
}

//Endpoint for allowing GHH-UI to post alerts to slack (if enabled in config)
func (ws *WebServer) CreateUiAlert(w http.ResponseWriter, r *http.Request) {
	if ws.EnableUiSlackAlerting {
		ws.beginAlert(w, r, ws.uiWebhookUrl, "GHH-UI")
	}
}

//Endpoint for allowing Data Science applications to post alerts to slack (if enabled in config)
func (ws *WebServer) CreateDsAlert(w http.ResponseWriter, r *http.Request) {
	if ws.EnableDsSlackAlerting {
		ws.beginAlert(w, r, ws.dsWebhookUrl, "Data Science")
	}
}

//GRPC Endpoint for allowing Core-Api to post alerts to slack
func (ws *WebServer) CreateApiAlert(ctx context.Context, request *pb.AlertRequest) (*emptypb.Empty, error) {
	l := logging.LoggerFromContext(ctx)
	l.Debug("Creating Alert to push to Slack...", zap.String("appName", "Core-Api"))
	err := ws.AlertService.PushApiAlertToSlack(ctx, request.MetaData, ws.apiWebhookUrl, "Core-Api")
	if err != nil {
		l.Error("An error occurred trying to post an Alert to Slack!", zap.String("appName", "Core-Api"), zap.Error(err))
		return nil, err
	}
	return &emptypb.Empty{}, nil
}

func (ws *WebServer) beginAlert(w http.ResponseWriter, r *http.Request, webhookUrl string, appName string) {
	l := logging.LoggerFromContext(r.Context())

	var alertRequest pb.AlertRequest
	l.Debug("Creating Alert to push to Slack...", zap.String("appName", appName))

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&alertRequest); err != nil {
		l.Error("Caught an exception while deserializing Alert Request payload!", zap.Error(err))
		w.WriteHeader(400)
		return
	}
	defer r.Body.Close()
	err := ws.AlertService.PushApiAlertToSlack(r.Context(), alertRequest.MetaData, webhookUrl, appName)
	if err != nil {
		l.Error("An error occurred trying to post an Alert to Slack!", zap.String("appName", appName), zap.Error(err))
		w.WriteHeader(500)
		return
	}

	w.WriteHeader(200)
}
