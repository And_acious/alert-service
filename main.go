package main

import (
	"geniusrx.dev/geniusrx/alert-service/pb"
	"geniusrx.dev/geniusrx/go/grpc"
	"geniusrx.dev/geniusrx/go/logging"
	"geniusrx.dev/geniusrx/go/tracing"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"net"
	"net/http"
	"strings"
)

func startWebServer(ws *WebServer) {
	router := ws.Router()
	//TODO: When UI/Data Science starts integrating with Alert-Service - Setup Machine-to-Machine tokens respectively
	//handler := security.createJwtMiddleware(viper.GetString("auth.audience"), viper.GetString("auth.domain"))
	//router.Use(handler.Handler)

	zap.L().Info("Web server up and running")
	if err := http.ListenAndServe(":8006", router); err != nil {
		zap.L().Fatal("Web server failed for some reason!", zap.Error(err))
	}
}

func startGrpcServer(server *WebServer) {
	lis, err := net.Listen("tcp", ":9006")
	if err != nil {
		zap.L().Error("failed to listen", zap.Error(err))
	}

	grpcServer := grpc.NewServer()
	pb.RegisterAlertServiceServer(grpcServer, server)
	zap.L().Info("Grpc server up and running")
	if err := grpcServer.Serve(lis); err != nil {
		zap.L().Fatal("Grpc server failed for some reason!", zap.Error(err))
	}
}

func main() {
	viper.SetEnvPrefix("app")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	viper.SetDefault("slack.alert.api.url", "https://hooks.slack.com/services/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")
	viper.SetDefault("slack.alert.ui.url", "https://hooks.slack.com/services/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")
	viper.SetDefault("slack.alert.ds.url", "https://hooks.slack.com/services/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")
	viper.SetDefault("slack.enable.ui.alerting", true)
	viper.SetDefault("slack.enable.ds.alerting", true)

	logger := logging.New()
	logger.Info("Server Started!")

	shutDown := tracing.New()
	defer shutDown()

	a := NewAlertService()
	ws := NewWebServer(
		a,
		viper.GetString("slack.alert.ui.url"),
		viper.GetString("slack.alert.ds.url"),
		viper.GetString("slack.alert.api.url"),
		viper.GetBool("slack.enable.ui.alerting"),
		viper.GetBool("slack.enable.ds.alerting"),
	)

	go startGrpcServer(ws)
	startWebServer(ws)
}
