package main_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	main "geniusrx.dev/geniusrx/alert-service"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	l, _ := zap.NewDevelopment()
	zap.ReplaceGlobals(l)

	os.Exit(m.Run())
}

func TestWebServer_CreateUiAlertSuccess(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	rr := httptest.NewRecorder()
	r := ws.Router()
	reqBody, _ := json.Marshal(map[string]string{"Test": "This is a test!"})

	req, err := http.NewRequest("POST", "/alert/ui", bytes.NewBuffer(reqBody))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}

func TestWebServer_CreateUiAlertISE(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("an exception was thrown pushing ui alert"))

	rr := httptest.NewRecorder()
	r := ws.Router()
	reqBody, _ := json.Marshal(map[string]string{"Test": "This is a test!"})

	req, err := http.NewRequest("POST", "/alert/ui", bytes.NewBuffer(reqBody))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)
}

func TestWebServer_CreateUiAlertBadRequest(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	rr := httptest.NewRecorder()
	r := ws.Router()

	req, err := http.NewRequest("POST", "/alert/ui", bytes.NewBuffer([]byte("Test")))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)
}

func TestWebServer_CreateDsAlertSuccess(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	rr := httptest.NewRecorder()
	r := ws.Router()
	reqBody, _ := json.Marshal(map[string]string{"Test": "This is a test!"})

	req, err := http.NewRequest("POST", "/alert/ds", bytes.NewBuffer(reqBody))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)
}

func TestWebServer_CreateDsAlertISE(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("an exception was thrown pushing ds alert"))

	rr := httptest.NewRecorder()
	r := ws.Router()
	reqBody, _ := json.Marshal(map[string]string{"Test": "This is a test!"})

	req, err := http.NewRequest("POST", "/alert/ds", bytes.NewBuffer(reqBody))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusInternalServerError, rr.Code)
}

func TestWebServer_CreateDsAlertBadRequest(t *testing.T) {
	m := &mockAlertService{}
	ws := main.WebServer{
		AlertService:          m,
		EnableDsSlackAlerting: true,
		EnableUiSlackAlerting: true,
	}

	m.On("PushApiAlertToSlack", mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("an exception was thrown pushing ds alert"))

	rr := httptest.NewRecorder()
	r := ws.Router()

	req, err := http.NewRequest("POST", "/alert/ds", bytes.NewBuffer([]byte("Test")))
	assert.NoError(t, err)

	r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)
}

// Mock Alert Service
type mockAlertService struct {
	mock.Mock
}

func (m *mockAlertService) PushApiAlertToSlack(ctx context.Context, alertMetaData map[string]string, alertWebhookUrl string, alertingApplicationName string) error {
	args := m.Called(ctx, alertMetaData, alertWebhookUrl, alertingApplicationName)
	return args.Error(0)
}

func (m *mockAlertService) ComposeSlackMessageBlocks(metaData map[string]string) []main.SlackMessageBlock {
	args := m.Called(metaData)
	return args.Get(0).([]main.SlackMessageBlock)
}
