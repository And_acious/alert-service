package main_test

import (
	"context"
	main "geniusrx.dev/geniusrx/alert-service"
	"reflect"
	"testing"
)

func Test_alertService_PushApiAlertToSlack(t *testing.T) {
	a := main.NewAlertService()
	type args struct {
		alertMetaData           map[string]string
		alertWebhookUrl         string
		alertingApplicationName string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Should successfully push Alert to Slack",
			args: args{
				alertMetaData: map[string]string{
					"Exception Message": "This is an alert from an automated test!",
					"Stack Trace":       "com.auth0.net.ExtendedBaseRequest.createResponseException(ExtendedBaseRequest.java:128)",
					"Span Id":           "64d3035701248e58",
					"Trace Id":          "64d3035701248e58",
					"Request":           "https://app.geniusrx.com/user/exists/test@test!com",
					"HTTP Method":       "POST",
				},
				alertWebhookUrl:         "https://hooks.slack.com/services/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",
				alertingApplicationName: "Core-Api",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		if err := a.PushApiAlertToSlack(context.Background(), tt.args.alertMetaData, tt.args.alertWebhookUrl, tt.args.alertingApplicationName); (err != nil) != tt.wantErr {
			t.Errorf("PushApiAlertToSlack() error = %v, wantErr %v", err, tt.wantErr)
		}
	}
}

func Test_alertService_ComposeSlackMessageBlocks(t *testing.T) {
	a := main.NewAlertService()
	blockList := make([]main.SlackMessageBlock, 0)
	blockList = append(blockList, main.SlackMessageBlock{
		Type: "section",
		Text: main.SlackMessageText{
			Type: "mrkdwn",
			Text: "*@channel Exception Message*: ```This is a test!```",
		},
	})

	blockList = append(blockList, main.SlackMessageBlock{
		Type: "section",
		Text: main.SlackMessageText{
			Type: "mrkdwn",
			Text: "*Stack Trace*: ```Something else!```",
		},
	})

	type args struct {
		metaData map[string]string
	}
	tests := []struct {
		name string
		args args
		want []main.SlackMessageBlock
	}{
		{
			name: "Should return list of Slack Message Blocks",
			args: args{metaData: map[string]string{"Exception Message": "This is a test!", "Stack Trace": "Something else!"}},
			want: blockList,
		},
	}
	for _, tt := range tests {
		if got := a.ComposeSlackMessageBlocks(tt.args.metaData); !reflect.DeepEqual(got, tt.want) {
			t.Errorf("ComposeSlackMessageBlocks() = %v, want %v", got, tt.want)
		}
	}
}

func TestAlertService_ComposeSlackMessageBlocksWithHTMLInMessage(t *testing.T) {
	a := main.NewAlertService()
	blockList := make([]main.SlackMessageBlock, 0)
	blockList = append(blockList, main.SlackMessageBlock{
		Type: "section",
		Text: main.SlackMessageText{
			Type: "mrkdwn",
			Text: "*@channel Exception Message*: ```com.auth0.exception.APIException: Request failed with status code 530: u003c!DOCTYPE htmlu003e u003c!--[if lt IE 7]u003e u003chtml class=\n        no-js\n        ie6\n        oldie\n         lang=```",
		},
	})

	blockList = append(blockList, main.SlackMessageBlock{
		Type: "section",
		Text: main.SlackMessageText{
			Type: "mrkdwn",
			Text: "*Stack Trace*: ```Something else!```",
		},
	})

	type args struct {
		metaData map[string]string
	}
	tests := []struct {
		name string
		args args
		want []main.SlackMessageBlock
	}{
		{
			name: "Should return list of Slack Message Blocks With HTML quotes removed",
			args: args{metaData: map[string]string{"Exception Message": "com.auth0.exception.APIException: Request failed with status code 530: u003c!DOCTYPE htmlu003e u003c!--[if lt IE 7]u003e u003chtml class=\"\n        no-js\n        ie6\n        oldie\n        \" lang=\"", "Stack Trace": "Something else!"}},
			want: blockList,
		},
	}
	for _, tt := range tests {
		if got := a.ComposeSlackMessageBlocks(tt.args.metaData); !reflect.DeepEqual(got, tt.want) {
			t.Errorf("ComposeSlackMessageBlocks() = %v, want %v", got, tt.want)
		}
	}
}
